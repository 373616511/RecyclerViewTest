package com.example.admin.testapplication;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.testapplication.entity.DishCatViewDto;
import com.example.admin.testapplication.entity.DishViewDto;

import java.util.List;

public class CatAdapter extends RecyclerView.Adapter<CatAdapter.VH> {

    //② 创建ViewHolder
    public static class VH extends RecyclerView.ViewHolder {
        public final TextView title;
        public final RecyclerView recyclerView;

        public VH(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.item1);
            recyclerView = v.findViewById(R.id.recyclerView2);
        }
    }

    private List<DishCatViewDto> mDatas;

    public CatAdapter(List<DishCatViewDto> data) {
        this.mDatas = data;
    }

    //在Adapter中实现3个方法
    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.title.setText(mDatas.get(position).getCatName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //item 点击事件
            }
        });

        //列表
        List<DishViewDto> dishViewDtoList = mDatas.get(position).getDishViewDtoList();
        DishAdapter dishAdapter = new DishAdapter(dishViewDtoList);

        //给分类的RecyclerView设置菜品数据适配器
        holder.recyclerView.setAdapter(dishAdapter);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(holder.recyclerView.getContext()));
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        //LayoutInflater.from指定写法
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_1, parent, false);
        return new VH(v);
    }

    public void setmDatas(List<DishCatViewDto> mDatas) {
        this.mDatas = mDatas;
        this.notifyDataSetChanged();
    }
}
