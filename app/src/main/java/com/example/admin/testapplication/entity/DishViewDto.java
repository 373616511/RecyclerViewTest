package com.example.admin.testapplication.entity;

public class DishViewDto {

    private String pid;

    private Integer isSelected;

    private Integer isHighestSale;

    private Integer status;

    private String dishCatId;

    private String dishCatName;

    private String dishName;

    private Integer price;

    private String unitId;

    private String unitName;

    private Integer isDiscount;

    private Integer timeConsuming;

    private Integer isEditPrice;

    private Integer isAuthority;

    private Integer isLargess;

    private Integer isPackage;

    private Integer isSalesVolume;

    private Integer memberPrice;

    private Integer orderNum;

    private Integer orderUnit;

    private Integer isHall;

    private String imgMain;

    private String imgStart;

    private String imgDetail;

    private String imgEnd;

    private String shopId;

    private Integer isRecommend;

    private String detail;

    private Integer sort;

    private Integer saleNum;//销量

    public Integer getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(Integer saleNum) {
        this.saleNum = saleNum;
    }

    public Integer getIsSelected() {
        return isSelected == null ? 0 : isSelected;
    }

    public void setIsSelected(Integer isSelected) {
        this.isSelected = isSelected;
    }

    public Integer getIsHighestSale() {
        return isHighestSale == null ? 0 : isHighestSale;
    }

    public void setIsHighestSale(Integer isHighestSale) {
        this.isHighestSale = isHighestSale;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDishCatId() {
        return dishCatId;
    }

    public void setDishCatId(String dishCatId) {
        this.dishCatId = dishCatId;
    }

    public String getDishCatName() {
        return dishCatName;
    }

    public void setDishCatName(String dishCatName) {
        this.dishCatName = dishCatName;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Integer getIsDiscount() {
        return isDiscount;
    }

    public void setIsDiscount(Integer isDiscount) {
        this.isDiscount = isDiscount;
    }

    public Integer getTimeConsuming() {
        return timeConsuming;
    }

    public void setTimeConsuming(Integer timeConsuming) {
        this.timeConsuming = timeConsuming;
    }

    public Integer getIsEditPrice() {
        return isEditPrice;
    }

    public void setIsEditPrice(Integer isEditPrice) {
        this.isEditPrice = isEditPrice;
    }

    public Integer getIsAuthority() {
        return isAuthority;
    }

    public void setIsAuthority(Integer isAuthority) {
        this.isAuthority = isAuthority;
    }

    public Integer getIsLargess() {
        return isLargess;
    }

    public void setIsLargess(Integer isLargess) {
        this.isLargess = isLargess;
    }

    public Integer getIsPackage() {
        return isPackage;
    }

    public void setIsPackage(Integer isPackage) {
        this.isPackage = isPackage;
    }

    public Integer getIsSalesVolume() {
        return isSalesVolume;
    }

    public void setIsSalesVolume(Integer isSalesVolume) {
        this.isSalesVolume = isSalesVolume;
    }

    public Integer getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(Integer memberPrice) {
        this.memberPrice = memberPrice;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getOrderUnit() {
        return orderUnit;
    }

    public void setOrderUnit(Integer orderUnit) {
        this.orderUnit = orderUnit;
    }

    public Integer getIsHall() {
        return isHall;
    }

    public void setIsHall(Integer isHall) {
        this.isHall = isHall;
    }

    public String getImgMain() {
        return imgMain;
    }

    public void setImgMain(String imgMain) {
        this.imgMain = imgMain;
    }

    public String getImgStart() {
        return imgStart;
    }

    public void setImgStart(String imgStart) {
        this.imgStart = imgStart;
    }

    public String getImgDetail() {
        return imgDetail;
    }

    public void setImgDetail(String imgDetail) {
        this.imgDetail = imgDetail;
    }

    public String getImgEnd() {
        return imgEnd;
    }

    public void setImgEnd(String imgEnd) {
        this.imgEnd = imgEnd;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public Integer getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(Integer isRecommend) {
        this.isRecommend = isRecommend;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}