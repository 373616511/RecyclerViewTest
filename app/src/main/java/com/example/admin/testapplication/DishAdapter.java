package com.example.admin.testapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.testapplication.MyDefination.MyImageView;
import com.example.admin.testapplication.entity.DishViewDto;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class DishAdapter extends RecyclerView.Adapter<DishAdapter.VH2> {

    private static final Object TAG = "商品列表适配器";

    //创建ViewHolder
    public static class VH2 extends RecyclerView.ViewHolder {

        private final TextView dishName;
        private final MyImageView imageView;
        private final TextView price;

        public VH2(View v) {
            super(v);
            dishName = (TextView) v.findViewById(R.id.dishName);
            imageView = v.findViewById(R.id.imageView);
            price = v.findViewById(R.id.price);
        }
    }

    private List<DishViewDto> mDatas;

    public DishAdapter(List<DishViewDto> data) {
        this.mDatas = data;
    }

    @Override
    public void onBindViewHolder(VH2 holder, int position) {
        DishViewDto dishViewDto = mDatas.get(position);
        String dishName = dishViewDto.getDishName();
        //设置标题
        holder.dishName.setText(dishName);
        //设置图片
        String imgUrl = "http://okyd.oss-cn-beijing.aliyuncs.com/img/156463992120242269.jpg";
        //从数据中获取数据
        String imgMain = dishViewDto.getImgMain();
        if (imgMain != null) {
            String[] split = imgMain.split(",");
            for (String s1 : split) {
                if (s1 != null && s1.length() > 0) {
                    imgUrl = s1;
                    break;
                }
            }
        }
        //自定义的ImageView实现网络图片
        holder.imageView.setImageURL(imgUrl);
        //设置价格
        holder.price.setText("￥" + dishViewDto.getPrice() + "/" + dishViewDto.getUnitName());
        //点击事件
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //item 点击事件
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    @Override
    public VH2 onCreateViewHolder(ViewGroup parent, int viewType) {
        //LayoutInflater.from指定写法
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_2, parent, false);
        return new VH2(v);
    }


    /**
     * 从服务器取图片
     * http://bbs.3gstdy.com
     *
     * @param url
     * @return
     */
    public static Bitmap getHttpBitmap(String url) {
        URL myFileUrl = null;
        Bitmap bitmap = null;
        try {
            myFileUrl = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
            conn.setConnectTimeout(0);
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
