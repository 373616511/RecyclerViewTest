package com.example.admin.testapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.testapplication.entity.DishCatViewDto;
import com.example.admin.testapplication.entity.ReponseEntity;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private TextView textView;

    private CatAdapter normalAdapter;

    private List<DishCatViewDto> list;

    private Gson gson;

    private RecyclerView recyclerView;

    private Button btnOne;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.text);
        gson = new Gson();
        list = new ArrayList<>();

        normalAdapter = new CatAdapter(list);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setAdapter(normalAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        btnOne = (Button) findViewById(R.id.btnOne);
        btnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "点击", Toast.LENGTH_SHORT).show();
                //initData();
            }
        });
        initDataLocal();
    }

    private void initDataLocal() {
        ReponseEntity reponseEntity = gson.fromJson(a, ReponseEntity.class);
        List<DishCatViewDto> result = reponseEntity.getResult();
        normalAdapter.setmDatas(result);
    }

    private void initData() {
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIzMmJkNjFkZTRlN2Q0NDQ1YjY0MWEyODg0ZDcwZTVlZiIsImlhdCI6MTU2NDM3MTE0Miwic3ViIjoic2hvcF91c2VyIiwiaXNzIjoid3d3Lm9rY2xvdWQuY29tIiwiZXhwIjoxNTY0OTc1OTQyfQ.xEvxYh1lNrqttv0UnBMcmmF9cX42QAMU7G0e7NDOXfU";
        String shopId = "1";
        String salesId = "2";
        String url = "http://192.168.1.64:8083/okcloud/dish/sales/listApp";

        FormBody build = new FormBody.Builder()
                .add("shopId", shopId)
                .add("salesId", salesId)
                .build();

        Request request = new Request.Builder()
                .addHeader("OKCLOUD-TOKEN", token)
                .url(url)
                .post(build)
                .build();

        //创建OkHttpClient对象
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
//                .writeTimeout(5, TimeUnit.SECONDS)
//                .readTimeout(5, TimeUnit.SECONDS)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "onFailure: " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d(TAG, response.protocol() + " " + response.code() + " " + response.message());
                Headers headers = response.headers();
                for (int i = 0; i < headers.size(); i++) {
                    Log.d(TAG, headers.name(i) + ":" + headers.value(i));
                }
                final String str = response.body().string();

                Log.d(TAG, "onResponse: " + str);

                //List<DishCatViewDto> dishCatViewDtoList = new ArrayList<>();
                ReponseEntity reponseEntity = gson.fromJson(str, ReponseEntity.class);
                list = reponseEntity.getResult();
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "code: ");
                        //Toast.makeText(MainActivity.this, str, Toast.LENGTH_SHORT).show();
                        normalAdapter.setmDatas(list);
                    }
                });
            }
        });
    }


    private final String a = "{\n" +
            "  \"code\": 200,\n" +
            "  \"message\": \"Success\",\n" +
            "  \"result\": [\n" +
            "    {\n" +
            "      \"pid\": \"1\",\n" +
            "      \"catName\": \"主食\",\n" +
            "      \"shopId\": \"1\",\n" +
            "      \"catType\": 0,\n" +
            "      \"sort\": 0,\n" +
            "      \"dishViewDtoList\": [\n" +
            "        {\n" +
            "          \"pid\": \"1b31ba7ec6e64adb89f4b505732323c1\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"嗷嗷叫什么名好\",\n" +
            "          \"price\": 2500,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": null,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": null,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 2,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": null,\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": null,\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": null,\n" +
            "          \"sort\": 0,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"4f887f061d0a44d487b12a2450c0e3fd\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"惊喜交加下\",\n" +
            "          \"price\": 2500,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": null,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": null,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 5,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": null,\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": null,\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": null,\n" +
            "          \"sort\": 0,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"6eaf2c24871648029036242a9f46a709\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"惊喜交加下单\",\n" +
            "          \"price\": 2500,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": null,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": null,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 5,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": null,\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": null,\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": null,\n" +
            "          \"sort\": 0,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"bded324ee315422b8718ac4c5a657b72\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"煎饼摊了的东西\",\n" +
            "          \"price\": 2000,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": null,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": null,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 5,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": null,\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": null,\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": null,\n" +
            "          \"sort\": 0,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"bf62dc7fb4df4c5c89aa8507d5b39fa7\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"嗷嗷\",\n" +
            "          \"price\": 2500,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": null,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 2000,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 2,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156464570723524591.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464570727172299.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464570727012721.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464570727096973.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464570738843417.jpeg\",\n" +
            "          \"imgStart\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156464570747038322.jpeg\",\n" +
            "          \"imgDetail\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156464570738709068.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464570742784855.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464570741186831.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464570745420316.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464570745175582.jpeg\",\n" +
            "          \"imgEnd\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156464570751233615.jpeg\",\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": \"这里没有\",\n" +
            "          \"sort\": 0,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"d7db9e016e904143b3ad1a7e536970e3\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"最全商品\",\n" +
            "          \"price\": 2500,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": 30,\n" +
            "          \"isEditPrice\": 1,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 1,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 2000,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 25,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156464904629724921.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464904630197351.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464904631167399.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464904630534476.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464904672435847.jpeg\",\n" +
            "          \"imgStart\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156464904681729195.jpeg\",\n" +
            "          \"imgDetail\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156464904674020699.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464904675674035.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464904675685898.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156464904679493104.jpeg\",\n" +
            "          \"imgEnd\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156464904682517166.jpeg\",\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": \"你就可以看到很多时候你\",\n" +
            "          \"sort\": 0,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"df51c7fa0b7d4826b1b9b3d1e71e48a1\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"嗷嗷哦嗷嗷嗷嗷\",\n" +
            "          \"price\": 5000,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": null,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": null,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 25,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": null,\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": null,\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": null,\n" +
            "          \"sort\": 0,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"e10d8f07d5bc4990aa8f13de332f4ef2\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"煎饼了个电话打\",\n" +
            "          \"price\": 2500,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": null,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": null,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 2,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": null,\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": null,\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": null,\n" +
            "          \"sort\": 0,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"e2cab251cc1d493f88b03f53eb840416\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"煎饼了吗！\",\n" +
            "          \"price\": 25300,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": null,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 2500,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 2,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": null,\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": null,\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": null,\n" +
            "          \"sort\": 0,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"eb69da7dcad247268c11b9023be62e60\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 0,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"鸭腿\",\n" +
            "          \"price\": 0,\n" +
            "          \"unitId\": \"1\",\n" +
            "          \"unitName\": \"份\",\n" +
            "          \"isDiscount\": 0,\n" +
            "          \"timeConsuming\": 0,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 0,\n" +
            "          \"isLargess\": 0,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 0,\n" +
            "          \"memberPrice\": 0,\n" +
            "          \"orderNum\": 0,\n" +
            "          \"orderUnit\": 1,\n" +
            "          \"isHall\": 0,\n" +
            "          \"imgMain\": \"string\",\n" +
            "          \"imgStart\": \"string\",\n" +
            "          \"imgDetail\": \"string\",\n" +
            "          \"imgEnd\": \"string\",\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": \"string\",\n" +
            "          \"sort\": 0,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"4905b3acec964d73941021e24c979d2a\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 0,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"鱼香肉丝\",\n" +
            "          \"price\": 3325,\n" +
            "          \"unitId\": \"1\",\n" +
            "          \"unitName\": \"份\",\n" +
            "          \"isDiscount\": 0,\n" +
            "          \"timeConsuming\": 0,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 0,\n" +
            "          \"isLargess\": 0,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 0,\n" +
            "          \"orderNum\": 0,\n" +
            "          \"orderUnit\": 1,\n" +
            "          \"isHall\": 0,\n" +
            "          \"imgMain\": \"string\",\n" +
            "          \"imgStart\": \"string\",\n" +
            "          \"imgDetail\": \"string\",\n" +
            "          \"imgEnd\": \"string\",\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": \"string\",\n" +
            "          \"sort\": 300,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"707dbc2fa04c4998969938e976442511\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"皮蛋豆腐\",\n" +
            "          \"price\": 123,\n" +
            "          \"unitId\": \"1\",\n" +
            "          \"unitName\": \"份\",\n" +
            "          \"isDiscount\": 0,\n" +
            "          \"timeConsuming\": 0,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 0,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 0,\n" +
            "          \"orderNum\": 0,\n" +
            "          \"orderUnit\": 1,\n" +
            "          \"isHall\": 0,\n" +
            "          \"imgMain\": null,\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": null,\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 1,\n" +
            "          \"detail\": null,\n" +
            "          \"sort\": 300,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"af7a52ea7ed34fa9af23f2eb11da179e\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 0,\n" +
            "          \"dishCatId\": \"1\",\n" +
            "          \"dishCatName\": \"主食\",\n" +
            "          \"dishName\": \"煎饼果子\",\n" +
            "          \"price\": 2145,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 0,\n" +
            "          \"timeConsuming\": 0,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 0,\n" +
            "          \"isLargess\": 0,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 0,\n" +
            "          \"orderNum\": 0,\n" +
            "          \"orderUnit\": 1,\n" +
            "          \"isHall\": 0,\n" +
            "          \"imgMain\": null,\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": null,\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": null,\n" +
            "          \"sort\": 300,\n" +
            "          \"saleNum\": 99,\n" +
            "          \"catType\": 0\n" +
            "        }\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"pid\": \"0ba23947372344ac81c551af36d2a91e\",\n" +
            "      \"catName\": \"酒水饮料\",\n" +
            "      \"shopId\": \"1\",\n" +
            "      \"catType\": 0,\n" +
            "      \"sort\": 100,\n" +
            "      \"dishViewDtoList\": [\n" +
            "        {\n" +
            "          \"pid\": \"41e7d3361fae4aeda6c98c7f17b85ff6\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"0ba23947372344ac81c551af36d2a91e\",\n" +
            "          \"dishCatName\": \"酒水饮料\",\n" +
            "          \"dishName\": \"酒水饮料\",\n" +
            "          \"price\": 2500,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": 30,\n" +
            "          \"isEditPrice\": 1,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 1,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 2000,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 2,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156457218006078180.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457218011380516.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457218012523228.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457218011717128.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457218010320619.jpeg\",\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156457218013413975.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457218016210718.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457218019511558.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457218018333819.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457218017178253.jpeg\",\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": \"大量生产经营和投资\",\n" +
            "          \"sort\": 300,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"679a983a9de14a429711f6d970ce146d\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"0ba23947372344ac81c551af36d2a91e\",\n" +
            "          \"dishCatName\": \"酒水饮料\",\n" +
            "          \"dishName\": \"煎饼\",\n" +
            "          \"price\": 2500,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": 2,\n" +
            "          \"isEditPrice\": 1,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 1,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 2000,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 2,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156455223138346127.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156455223140283742.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156455223138397417.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156455223139069662.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156455223144076222.jpeg\",\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156455223144473129.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156455223147147064.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156455223146014874.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156455223150673444.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156455223150908162.jpeg\",\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": \"嗷嗷\",\n" +
            "          \"sort\": 300,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"adacc3d6c4874516ac0e8fc8cc2a7708\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"0ba23947372344ac81c551af36d2a91e\",\n" +
            "          \"dishCatName\": \"酒水饮料\",\n" +
            "          \"dishName\": \"udjjdjd\",\n" +
            "          \"price\": 2500,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": 30,\n" +
            "          \"isEditPrice\": 1,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 1,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 2000,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 2,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": null,\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": null,\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": null,\n" +
            "          \"sort\": 300,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"ce86fa638e564770af38efcfb8b658f7\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"0ba23947372344ac81c551af36d2a91e\",\n" +
            "          \"dishCatName\": \"酒水饮料\",\n" +
            "          \"dishName\": \"嗷嗷哭aaa\",\n" +
            "          \"price\": 2000,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": null,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 0,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 2,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156457131318537481.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457131323928430.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457131326282746.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457131325535856.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457131325831920.jpeg\",\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156457131331055273.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457131334812783.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457131332615636.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457131333865896.jpeg\",\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": \"这里没有\",\n" +
            "          \"sort\": 300,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"d760c3d837a649b2909ab1be0ee10df8\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"0ba23947372344ac81c551af36d2a91e\",\n" +
            "          \"dishCatName\": \"酒水饮料\",\n" +
            "          \"dishName\": \"煎饼和她\",\n" +
            "          \"price\": 25800,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": null,\n" +
            "          \"isEditPrice\": 0,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 0,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": null,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 2,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156457176745384339.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457176745233988.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457176745510095.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457176747317857.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457176750830947.jpeg\",\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156457176753109520.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457176754251372.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457176752668897.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457176756925681.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457176756450327.jpeg\",\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": \"你也快来测测异性眼中\",\n" +
            "          \"sort\": 300,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        },\n" +
            "        {\n" +
            "          \"pid\": \"d863625914b24bd9815320d3bebca214\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"0ba23947372344ac81c551af36d2a91e\",\n" +
            "          \"dishCatName\": \"酒水饮料\",\n" +
            "          \"dishName\": \"你说了一个好\",\n" +
            "          \"price\": 2500,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": 30,\n" +
            "          \"isEditPrice\": 1,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 1,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 2000,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 25,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156457202528995603.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457202530741830.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457202528981677.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457202530078785.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457202533219141.jpeg\",\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156457202536902531.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457202534674663.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457202535013165.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457202537357569.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457202537981320.jpeg\",\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": \"要犹犹豫豫\",\n" +
            "          \"sort\": 300,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        }\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"pid\": \"3\",\n" +
            "      \"catName\": \"炒菜\",\n" +
            "      \"shopId\": \"1\",\n" +
            "      \"catType\": 0,\n" +
            "      \"sort\": 100,\n" +
            "      \"dishViewDtoList\": [\n" +
            "        {\n" +
            "          \"pid\": \"b7c22cbf331840648f305403ffe01b76\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"3\",\n" +
            "          \"dishCatName\": \"炒菜\",\n" +
            "          \"dishName\": \"大炒菜\",\n" +
            "          \"price\": 2500,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": 30,\n" +
            "          \"isEditPrice\": 1,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 1,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 2000,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 25,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156457236154711040.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457236159584560.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457236159154306.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457236158026812.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457236159799573.jpeg\",\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": \"http://okyd.oss-cn-beijing.aliyuncs.com/img/156457236163845859.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457236165091011.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457236165496184.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457236165099360.jpeg,http://okyd.oss-cn-beijing.aliyuncs.com/img/156457236168646852.jpeg\",\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": \"这里了。煎饼了吗\",\n" +
            "          \"sort\": 300,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        }\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"pid\": \"d467bbc51bdb469a870d999ac2c01422\",\n" +
            "      \"catName\": \"主食2\",\n" +
            "      \"shopId\": \"1\",\n" +
            "      \"catType\": 0,\n" +
            "      \"sort\": 100,\n" +
            "      \"dishViewDtoList\": [\n" +
            "        {\n" +
            "          \"pid\": \"1fc5fb97feb840bf95977e7e45a7e5ce\",\n" +
            "          \"isSelected\": 0,\n" +
            "          \"isHighestSale\": 0,\n" +
            "          \"status\": 1,\n" +
            "          \"dishCatId\": \"d467bbc51bdb469a870d999ac2c01422\",\n" +
            "          \"dishCatName\": \"主食2\",\n" +
            "          \"dishName\": \"大小护士\",\n" +
            "          \"price\": 2500,\n" +
            "          \"unitId\": \"2\",\n" +
            "          \"unitName\": \"盘\",\n" +
            "          \"isDiscount\": 1,\n" +
            "          \"timeConsuming\": 30,\n" +
            "          \"isEditPrice\": 1,\n" +
            "          \"isAuthority\": 1,\n" +
            "          \"isLargess\": 1,\n" +
            "          \"isPackage\": 1,\n" +
            "          \"isSalesVolume\": 1,\n" +
            "          \"memberPrice\": 2000,\n" +
            "          \"orderNum\": 1,\n" +
            "          \"orderUnit\": 2,\n" +
            "          \"isHall\": 1,\n" +
            "          \"imgMain\": null,\n" +
            "          \"imgStart\": null,\n" +
            "          \"imgDetail\": null,\n" +
            "          \"imgEnd\": null,\n" +
            "          \"shopId\": \"1\",\n" +
            "          \"isRecommend\": 0,\n" +
            "          \"detail\": null,\n" +
            "          \"sort\": 300,\n" +
            "          \"saleNum\": 0,\n" +
            "          \"catType\": 0\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ]\n" +
            "}";
}
