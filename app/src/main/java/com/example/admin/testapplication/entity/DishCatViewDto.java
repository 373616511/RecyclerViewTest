package com.example.admin.testapplication.entity;

import java.util.List;

public class DishCatViewDto {

    private String pid;

    private String catName;

    private String shopId;

    private Integer catType;

    private Integer sort;

    private List<DishViewDto> dishViewDtoList;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public Integer getCatType() {
        return catType;
    }

    public void setCatType(Integer catType) {
        this.catType = catType;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public List<DishViewDto> getDishViewDtoList() {
        return dishViewDtoList;
    }

    public void setDishViewDtoList(List<DishViewDto> dishViewDtoList) {
        this.dishViewDtoList = dishViewDtoList;
    }
}