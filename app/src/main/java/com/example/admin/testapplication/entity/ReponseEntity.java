package com.example.admin.testapplication.entity;

import java.util.List;

public class ReponseEntity {

    private int code;

    private String message;

    private List<DishCatViewDto> result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DishCatViewDto> getResult() {
        return result;
    }

    public void setResult(List<DishCatViewDto> result) {
        this.result = result;
    }
}
